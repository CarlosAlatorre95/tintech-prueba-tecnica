## Prueba Técnica en React - Buscador de Música con Last.fm API
### Objetivo
Crear una aplicación web utilizando React que permita a los usuarios buscar música utilizando la API de Last.fm. La aplicación deberá mostrar los álbumes, canciones y artistas relacionados con la búsqueda realizada.

### Pasos a seguir

1. #### Configuración del Proyecto
   * Crea un nuevo proyecto de React utilizando tu método preferido.
   * Elige la base de datos de su preferencia.
   * Utilizar fetch para las consultas de api.
\
&nbsp;

2. #### Auth
   * Crear un login de correo y contraseña.
   * Crear un registro.
   * Proteger la aplicación para no acceder sin haber iniciado sesión.
\
&nbsp;
   

3. #### Integración con la API de Last.fm https://www.last.fm/es/api
   * Utiliza la siguiente apikey para la integración con Last.fm: 
   * API KEY = fcd492e73eaf5db3fc46164916b00df9
\
&nbsp;


4. #### Componentes Básicos
   * Crea un componente SearchBar que contendrá el campo de búsqueda.
   * Crea un componente SearchResults para mostrar los resultados de la búsqueda (álbumes, canciones y artistas).
   * Crea un componente ResultItem para mostrar un elemento individual (álbum, canción o artista) en la lista de resultados.
\
&nbsp;

5. #### Implementación de la Búsqueda
   * Mostrar resultados de artistas, canciones y álbumes en función de la consulta de búsqueda.
\
&nbsp;


6. #### Llamadas a la API y Mostrar Resultados
   * Cuando el usuario escriba en el campo de búsqueda, realiza una llamada a la API de Last.fm utilizando la URL adecuada, incluyendo la búsqueda y tu API Key.
   * Procesa los resultados de la API y actualiza el estado de tu componente SearchResults.
\
&nbsp;


7. #### Diseño y Estilos
   * Agrega estilos CSS para mejorar la apariencia de la aplicación.
   * Crea una interfaz de usuario intuitiva y receptiva.
\
&nbsp;


8. #### Pruebas y Validación
   * Prueba exhaustivamente la aplicación para asegurarte de que funciona correctamente.
   * Asegúrate de que la búsqueda, la API y la actualización de resultados se comporten según lo esperado.
\
&nbsp;


9. #### Funciones adicionales
   * Implementa paginación para mostrar más resultados.
   * Asegúrate de que los componentes se vean bien en diferentes dispositivos y tamaños de pantalla.
\
&nbsp;

### Entrega
   * Crea commits por cada punto mencionado en este documento
   * Sube tu código a un repositorio en GitHub u otra plataforma de control de versiones.
   * Proporciona instrucciones claras en el archivo README.md sobre cómo instalar, ejecutar y utilizar tu aplicación.
\
&nbsp;

Este proyecto es una oportunidad para demostrar sus habilidades en el desarrollo de aplicaciones React JS y la integración con APIs externas. ¡Diviértase codificando!

¡Buena suerte!
